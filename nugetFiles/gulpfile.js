/*****************************************************************************************
* Nuget package build gulpfile. 
*****************************************************************************************/
const gulp    = require('gulp');
const common  = require('@slcpub/gulp-common') .useGulp(gulp);
const nuget   = require('@slcpub/gulp-nuget').useGulp(gulp);

common .addTasks();
nuget  .addTasks();

/*------------------------------------------*/
/* Include Doxygen tasks and configuration. */
/*------------------------------------------*/
const doxygen = require('@slcpub/gulp-doxygen').useGulp(gulp);
doxygen({ doxyDir:'./Docs', docsDir:'./Docs/output', senseDir:'./output' });
doxygen.addTasks();

/*-----------------*/
/* Show main help. */
/*-----------------*/
gulp.task('default', () => common.showUsage());
