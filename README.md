## Notes and Articles

  - [How to Build a NuGet Package Using npm][build-nuget-package]
  - [How to Build a Package for both npm and bower][build-npm-and-bower]
  - [How to Build Intellisense XML File from Doxygen XML Output][build-intellisense]

[build-intellisense]:  ./BuildIntellisense.md
[build-npm-and-bower]: ./BuildPackageForNPMandBower.md
[build-nuget-package]: ./BuildNuGetPackageUsingNPM.md