<!--****************************************************************************
* $Id: README.md 72ef34e 2022-04-09 11:40:46 -0700 sthames42 $
*****************************************************************************-->
## Building a Package for npm and Bower

Publishing a package to the [npm registry], so it may be installed using the [npm install] command, 
is done using the [npm publish] command. 

[It is no longer possible](https://github.com/bower/bower/issues/2558) to register packages with the 
public bower registry but it is possible to use a private registry with packages like [private-bower]. 
However, packages can be installed by bower directly from the package repository using [bower install].

This procedure describes building a package such that the files to be included can be published to
both an npm registry and to a private bower registry simply and easily. It uses a set of
[build tools][gulp-npm] designed specifically facilitate the process.

### How It Works

The build process creates the library files in the `dist` folder. `package.json` must be configured 
so that the `main` and `files` properties specify the files in the `dist/` folder to be included in 
the package. Before publishing, a modified `package.json` is copied to `dist/` such that "dist/" is 
removed from the `main` and `files` properties, making the file references relative to the `dist/`
folder. Only the contents of the `dist/` folder are included in the package published to the npm registry.

When a package is installed from Bower, the library files to install are retrieved from the source 
code repository. Therefore, the repository must include the contents of the `dist/` folder. The
`bower.json#ignore` property lists the files that are not part of the library package so the
[bower install] command copies only the relevant files.
 
`bower.json` must be configured with the `ignore` property such that only the `bower.json` file and
files in the `dist/` folder are installed to the `bower_components` folder by [bower install].

[bower install]: https://bower.io/#install-packages
[npm install]:   https://docs.npmjs.com/cli/v8/commands/npm-install

#### package.json

```json
{
  "name": "@scope/packageName",
  "description": "...",
  "version": "...",
  "main": "dist/mainFile(s)",
  "files": [
    "dist/otherFiles"
  ],
  "dependencies": {
    "node_modules": "dependencies"
  },
  "devDependencies": {
    "@slcpub/gulp-common": "^1.0.0",
    "@slcpub/gulp-npm": "^1.0.0"
  },
  "helpDocDependencies": [
    "@slcpub/gulp-common",
    "@slcpub/gulp-npm"
  ],
  "scripts": {
    "build": "Build commands",
    "prepublishOnly": "node -e \"console.log('ERROR: use \\\"gulp publish\\\"\\n'\") && exit 1",
    "test": "Test commands",
    "preversion": "npm test",
    "version": "npm run build && gulp update-bower.json && git add -A bower.json dist"
  },
  "private": true,
  "publishConfig": {
     "@scope:registry": "Registry endpoint URL."
  }
}
```
  - **`name : "@scope/packageName"`**  
    [`@scope`][npm scope] is read by **`gulp set-registry`** to use in finding the 
    `publishConfig#@scope:registry` property containing the registry endpoint URL.

  - **`main`**  
    File(s) to include in the package to be installed by npm or bower. All files must reside in the 
    `dist/` folder.

  - **`files`**  
    Additional file(s) to include in the package to be installed by npm or bower. All must reside 
    in the `dist/` folder.

  - **`devDependencies`**  
    [gulp-common] and [gulp-npm] from [`@slcpub`][slcpub-registry] are required.

  - **`helpDocDependencies`**  
    Includes documentation from `@slcpub` packages in `gulp-common#showUsage` command.

  - **`scripts`**
  
    - **`prepublishOnly`**  
      Run by [npm publish] and should throw an error since `npm publish` is not used in the
      normal way. `gulp publish` publishes the files in the `dist/` folder.

    - **`preversion`**  
      Run by [npm version] and should run tests. Halts version command on failure.

    - **`test`**  
      Perform any automated tests.

    - **`version`**  
      Run by [npm version] after updating `package.json#version`. Builds the library, updates 
      `bower.json` from `package.json`, and stages modified files prior to version commit.

  - **`private : true`**  
    Prevents [npm publish] from publishing a package containing the root folder.
    This does not prevent the package from being built and is not necessary if
    `scripts#prepublishOnly` is set as above.

  - **`publishConfig`**  
    Includes the registry key for the package scope. Read by [set-registry](#set-registry)
    to set the [npm config] registry endpoint URL and authentication token.
  
    [Github will fail to publish with a 401 error](https://stackoverflow.com/a/64339511/2245849) 
    if the endpoint URL does not include a trailing slash (/).

#### bower.json

```json
{
  "name": "packageName",
  "description": "...",
  "version": "...",
  "main": [
    "dist/mainFile(s)"
  ],
  "dependencies": {
    "bower_components": "dependencies"
  },
  "ignore": [
    ".gitignore",
    "LICENSE",
    "package*.json",
    "README.md",
    "/*.js"
    "/src",
    "/tests",
    "Any other files that should not be installed."
  ],
}
```

  - **`name`**, **`description`**, **`version`**  
    `gulp update-bower.json` copies these properties from `package.json` as part of build process. 
    [Package scope][npm scope] is removed from the name.
    
  - **`main`**  
    Package entry-point files residing in `dist/` folder.
    
  - **`ignore`**  
    List of files to ignore when installing the package to **`bower_components`**. Only **`bower.json`**
    and the files listed in **`main`** should be installed. 


### Building and Publishing

  1) Set the endpoint for the `@slcpub` [package registry][slcpub-registry] using [npm].
  1) Install development dependencies:
     ```bash
     npm install
     ```

The package `gulpfile.js` must add the tasks from `gulp-common` and `gulp-npm`:
```js
const gulp   = require('gulp');
const common = require('@slcpub/gulp-common').useGulp(gulp);
const npm    = require('@slcpub/gulp-npm')   .useGulp(gulp);
common.addTasks();
npm   .addTasks();
```

After testing is complete and the package is ready to publish:

1) Run [**`npm version`**][npm version]
   - *preversion*: **Run library tests**,
   - Updates the version number in `package.json`,
   - *version*:
     - Build the library in the `dist/` folder.**
     - Run **`gulp update-bower.json`**. 
     - Stage `bower.json` and `dist` for commit,
   - Commits changes and creates a version tag.

1) If, on second thought, the package is not ready,
   * Run **`gulp undo-version`** to remove the tag and restore the repository.

1) If ready to publish,
   * Run **`gulp push`** to update the remote repo with the new version.

   ***The package may now be installed from the repo using bower.***

1) Run **`gulp set-registry`**, if necessary, to configure npm for publishing 
   to the package registry. This will require an access token.
   
1) Run **`gulp publish`** to publish the package to npm.
     - Run **`gulp build-package.json`**.

   ***The package may now be installed using npm.***

1) If publishing to a private bower registry created by the npm `private-bower` package,

   1) If the package repository is private and access token string has not been set, 
      run **`gulp set-bower-token`**, to set the access token string that must be 
      embedded in the package repository URL to retrieve the package from the repo.

   1) Run **`gulp publish-bower`** to publish the package to the private bower registry.

   ***The package may now be installed from the private bower registry.***


### Installation

#### npm

1) Set the package registry endpoint for your [package scope][npm scope].  Set the access token 
   for a private registry.
1) Add the package to `package.json#dependencies` with the [semantic version][semver] required. 
    ```json
    dependencies: {
      "@scope/packageName":"^1.0.27"
    }
    ```
1) Run `npm install`.

#### bower

1) Add the package to `bower.json#dependencies`.
   - If installing from the package repository, you must specify the repository endpoint URL including the version tag.
      ```json
      dependencies: {
        "packageName": "git+<repositoryURL>#v1.0.27",
      }
      ```
   - If installing from a private bower registry,
     Create a local `.bowerrc` file to contain the registry URL:
     ```json
     {
       registry: "registryHost"
     }
     ```
     Add the package to `bower.json#dependencies` just as with `package.json` but without the package scope:
     ```json
     dependencies: {
       "packageName":"^1.0.27"
     }
     ```
1) Run `bower install`.


## References

  - [Gulp]
  - [npm]
  - npm Packages in the Package Registry
    - [GitHub][github-registry]
      - [Personal Access Tokens][github-personal access token]
      - [Using Git over HTTPS and OAuth][github-oauth2]
    - [GitLab][gitlab-registry]
      - [Personal Access Tokens][gitlab-personal access token]
      - [Deploy Tokens][gitlab-deploy token]
      - [Access Git over HTTPS with access token][gitlab-oauth2]
  - [npm Package Scopes][npm scope]
  - [`npm version` command][npm version]
  - [bower.json specification][bower.json]
  - [The npm `private-bower` package][private-bower].

[gulp]:                         https://gulpjs.com/docs/en/getting-started/javascript-and-gulpfiles
[npm]:                          https://docs.npmjs.com/about-npm
[npm registry]:                 https://docs.npmjs.com/cli/v8/using-npm/registry
[npm scope]:                    https://docs.npmjs.com/cli/v6/using-npm/scope
[npm config]:                   https://docs.npmjs.com/cli/v8/commands/npm-config
[npm publish]:                  https://docs.npmjs.com/cli/v8/commands/npm-publish
[npm version]:                  https://docs.npmjs.com/cli/v6/commands/npm-version
[bower.json]:                   https://github.com/bower/spec/blob/master/json.md
[semver]:                       https://docs.npmjs.com/about-semantic-versioning
[semver versioning]:            https://semver.org/
[slcpub-registry]:              https://gitlab.com/slcon/pub/registry
[private-bower]:                https://www.npmjs.com/package/private-bower
[github-registry]:              https://docs.github.com/en/packages/working-with-a-github-packages-registry/working-with-the-npm-registry
[github-personal access token]: https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token
[github-oauth2]:                https://github.blog/2012-09-21-easier-builds-and-deployments-using-git-over-https-and-oauth/
[gitlab-registry]:              https://docs.gitlab.com/ee/user/packages/npm_registry/    
[gitlab-personal access token]: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#personal-access-tokens
[gitlab-deploy token]:          https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html
[gitlab-oauth2]:                https://docs.gitlab.com/ee/api/oauth2.html#access-git-over-https-with-access-token
[gulp-common]:                  https://gitlab.com/slcon/pub/build/gulp-common
[gulp-npm]:                     https://gitlab.com/slcon/pub/build/gulp-npm