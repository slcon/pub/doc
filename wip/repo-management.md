### Create a Repo with `trunk`

1) Create a new, blank project in the GitLab group.  
   ***Do not Initialize repository with a README.***  
   Adding a README will create the `main` branch, automatically.

1) Clone the repo and create the `trunk` branch.
   ```
   git clone https://gitlab.com/groupURL/projectPath.git
   cd projectPath
   git checkout -b trunk
   touch README.md
   git add README.md
   git commit -m "Create trunk."
   git push -u origin trunk
   ```
