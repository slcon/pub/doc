## General Information

A collection of notes and instructions. All files are for documentation only. There are 
no code files in this repo. 

### npm Package Registry

All the npm packages in the [Repos group][the group] are public and can be installed 
from the group [package registry][npm-registry]. Use the [Project-level endpoint][npm-endpoint] 
with [the group]'s **Group ID** in place of **<your_project_id>** to install npm 
packages from any project in the group.  

**No authentication is required**. 

Set the registry for the package scope using [npm config][npm-endpoint]
(Use the `-g` flag to set the registry globally) or with an ```.npmrc``` file
```sh
@slcpub:registry=<Project-level Endpoint>
```

### NuGet Package Registry

Although the [NuGet packages][nuget-registry] in the [Repos group][the group] are 
public, **GitLab rejects anonymous requests on the group-level endpoint**. It is 
therefore necessary to install these packages from the [project registry][nuget-registry]. 

Go to the project repo for the project in [the group] to get the **Project ID**
and replace **<your_project_id>** in the [Project-level endpoint][nuget-endpoint].

**No authentication is required**. 

Add the source for the package using the 
[NuGet CLI](https://docs.gitlab.com/ee/user/packages/nuget_repository/#project-level-endpoint),
the [.NET CLI](https://docs.gitlab.com/ee/user/packages/nuget_repository/#project-level-endpoint-2),
or with a [configuration file](https://docs.gitlab.com/ee/user/packages/nuget_repository/#project-level-endpoint-3).


## References

  - [Repos Group][the group]
  - [Package Registry](https://docs.gitlab.com/ee/user/packages/package_registry/)
  - [npm packages in the Package Registry][npm-registry]
  - [NuGet packages in the Package Registry][nuget-registry]


[the group]:     https://gitlab.com/slcon/public/repos
[npm-registry]:   https://docs.gitlab.com/ee/user/packages/npm_registry/
[npm-endpoint]:   https://docs.gitlab.com/ee/user/packages/npm_registry/#project-level-npm-endpoint
[nuget-registry]: https://docs.gitlab.com/ee/user/packages/nuget_repository/
[nuget-endpoint]: https://docs.gitlab.com/ee/user/packages/nuget_repository/#project-level-endpoint
